
# Common ReST Client Library

This project provides a library for Spring and POJO-based REST client instantiation.

## Usage

First, you will need to include the library in your project.

```xml
<project>
    ...
    <dependencies>
        ...
        <dependency>
            <groupId>com.inteligr8</groupId>
            <artifactId>common-rest-client</artifactId>
            <version>...</version>
        </dependency>
        ...
    </dependencies>
    ...
</project>
```

See the `cxf` and `jersey` branches for examples and more documentation.
