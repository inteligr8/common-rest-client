/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.rs;

import jakarta.ws.rs.core.Form;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the OAuth Client Credential flow as an authorization
 * filter.
 * 
 * @author brian@inteligr8.com
 */
public class OAuthClientCredentialAuthorizationFilter extends OAuthAuthorizationFilter {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * @param tokenUrl The URL to the OAuth IdP token service.
	 * @param clientId The ID provided by the OAuth IdP administrator.
	 * @param clientSecret The passcode provided by the OAuth IdP administrator.
	 */
	public OAuthClientCredentialAuthorizationFilter(String tokenUrl, String clientId, String clientSecret) {
		super(tokenUrl, clientId, clientSecret);
	}
	
	@Override
	protected Form createForm() {
        this.logger.debug("Using OAuth grant_type 'client_credentials'");
		return new Form().param("grant_type", "client_credentials");
	}
	
	@Override
	protected void extendFormSensitive(Form form) {
	}

}
