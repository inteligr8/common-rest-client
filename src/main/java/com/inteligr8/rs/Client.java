/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.rs;

import java.util.concurrent.TimeUnit;

import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jakarta.rs.json.JacksonJsonProvider;

/**
 * A class that provides pre-configured Jakarta RS Client &amp; WebTarget objects.
 * 
 * @author brian@inteligr8.com
 */
public abstract class Client {
	
	private final Object sync = new Object();
	private jakarta.ws.rs.client.Client client;
	
	/**
	 * This method retrieves the configuration for the client.
	 * 
	 * @return The client configuration.
	 */
	public abstract ClientConfiguration getConfig();
	
	/**
	 * This method retrieves an anonymous cached instance of the underlying
	 * Jakarta RS client.
	 * 
	 * @return A pre-configured Jakarta RS client (no URL) with configured authorization.
	 */
	public final jakarta.ws.rs.client.Client getClient() {
		synchronized (this.sync) {
			if (this.client == null)
				this.client = this.buildClient((AuthorizationFilter)null);
		}

		return this.client;
	}
	
	/**
	 * This method retrieves either an anonymous cached instance or builds an
	 * authorized instance of the underlying Jakarta RS client.
	 * 
	 * @param authFilter A dynamic authorization filter.
	 * @return A pre-configured Jakarta RS client (no URL) with the specified authorization.
	 */
	public jakarta.ws.rs.client.Client getClient(AuthorizationFilter authFilter) {
		if (authFilter == null) {
			return this.getClient();
		} else {
			return this.buildClient(authFilter);
		}
	}
	
	/**
	 * This method builds a new Jakarta RS client with optional authorization.
	 * 
	 * @param authFilter A dynamic authorization filter.
	 * @return A pre-configured Jakarta RS client (no URL) with the specified authorization.
	 */
	public final jakarta.ws.rs.client.Client buildClient(AuthorizationFilter authFilter) {
	    ObjectMapper om = new ObjectMapper();
	    om.registerModules(new JavaTimeModule());
	    this.getConfig().configureJacksonMapper(om);
	    
	    JacksonJsonProvider provider = new JacksonJsonProvider(om, JacksonJsonProvider.BASIC_ANNOTATIONS);
		this.getConfig().configureJacksonProvider(provider);
        
		if (this.getConfig().isWrapRootValueEnabled())
			provider.enable(SerializationFeature.WRAP_ROOT_VALUE);
		if (this.getConfig().isUnwrapRootValueEnabled())
			provider.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
		
		ClientBuilder clientBuilder = ClientBuilder.newBuilder()
				.register(provider)
				.register(new LoggingFilter());
		
		if (this.getConfig().getConnectTimeoutInMillis() != null)
		    clientBuilder.connectTimeout(this.getConfig().getConnectTimeoutInMillis(), TimeUnit.MILLISECONDS);
        if (this.getConfig().getResponseTimeoutInMillis() != null)
            clientBuilder.readTimeout(this.getConfig().getResponseTimeoutInMillis(), TimeUnit.MILLISECONDS);

		if (authFilter == null)
			authFilter = this.getConfig().createAuthorizationFilter();
		if (authFilter != null)
			clientBuilder.register(authFilter);
		this.buildClient(clientBuilder);
		this.getConfig().configureClient(clientBuilder);
		
		return clientBuilder.build();
	}
    
    /**
	 * This method allows sub-classes to extend the Jakarta RS client builder
	 * before the client is built.
	 * 
     * @param clientBuilder A Jakarta RS client builder.
     */
    public void buildClient(ClientBuilder clientBuilder) {
        // for extension purposes
    }

	/**
	 * This method builds an anonymous Jakarta RS target.
	 * 
	 * @return A pre-configured Jakarta RS target (client w/ base URL) with configured authorization.
	 */
	public final WebTarget getTarget() {
		return this.getClient()
				.target(this.getConfig().getBaseUrl());
	}

	/**
	 * This method builds an authorized Jakarta RS target.
	 * 
	 * @param authFilter A dynamic authorization filter.
	 * @return A pre-configured Jakarta RS target (client w/ base URL) with the specified authorization.
	 */
	public WebTarget getTarget(AuthorizationFilter authFilter) {
		if (authFilter == null) {
			return this.getTarget();
		} else {
			return this.getClient(authFilter)
					.target(this.getConfig().getBaseUrl());
		}
	}
	
	/**
	 * This method retrieves a Jakarta RS implementation of the specified API.
	 * 
	 * @param <T> A Jakarta RS annotated API class.
	 * @param apiClass A Jakarta RS annotated API class.
	 * @return An instance of the API class.
	 */
	public final <T> T getApi(Class<T> apiClass) {
		return this.getApi(null, apiClass);
	}

	/**
	 * This method retrieves a Jakarta RS implementation of the specified API with
	 * the specified authorization.
	 * 
	 * @param <T> A Jakarta RS annotated API class.
	 * @param authFilter A dynamic authorization filter.
	 * @param apiClass A Jakarta RS annotated API class.
	 * @return An instance of the API class.
	 */
	public abstract <T> T getApi(AuthorizationFilter authFilter, Class<T> apiClass);

}
