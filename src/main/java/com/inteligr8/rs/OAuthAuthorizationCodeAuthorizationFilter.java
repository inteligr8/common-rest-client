/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.rs;

import java.net.URI;

import jakarta.ws.rs.core.Form;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the OAuth Authorization Code flow as an authorization
 * filter.
 * 
 * @author brian@inteligr8.com
 */
public class OAuthAuthorizationCodeAuthorizationFilter extends OAuthAuthorizationFilter {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final String code;
	private final URI redirectUri;
	
	/**
	 * @param tokenUrl The URL to the OAuth IdP token service.
	 * @param clientId The ID provided by the OAuth IdP administrator.
	 * @param code The authorization code acquired from the OAuth IdP from a user outside the purview of this library.
	 */
	public OAuthAuthorizationCodeAuthorizationFilter(String tokenUrl, String clientId, String code) {
		this(tokenUrl, clientId, null, code);
	}

	/**
	 * @param tokenUrl The URL to the OAuth IdP token service.
	 * @param clientId The ID provided by the OAuth IdP administrator.
	 * @param code The authorization code acquired from the OAuth IdP from a user outside the purview of this library.
	 * @param redirectUri The URL for the OAuth IdP to redirect after successful authorization.
	 */
	public OAuthAuthorizationCodeAuthorizationFilter(String tokenUrl, String clientId, String code, URI redirectUri) {
		this(tokenUrl, clientId, null, code, redirectUri);
	}

	/**
	 * @param tokenUrl The URL to the OAuth IdP token service.
	 * @param clientId The ID provided by the OAuth IdP administrator.
	 * @param clientSecret The passcode provided by the OAuth IdP administrator.
	 * @param code The authorization code acquired from the OAuth IdP from a user outside the purview of this library.
	 */
	public OAuthAuthorizationCodeAuthorizationFilter(String tokenUrl, String clientId, String clientSecret, String code) {
		this(tokenUrl, clientId, clientSecret, code, null);
	}

	/**
	 * @param tokenUrl The URL to the OAuth IdP token service.
	 * @param clientId The ID provided by the OAuth IdP administrator.
	 * @param clientSecret The passcode provided by the OAuth IdP administrator.
	 * @param code The authorization code acquired from the OAuth IdP from a user outside the purview of this library.
	 * @param redirectUri The URL for the OAuth IdP to redirect after successful authorization.
	 */
	public OAuthAuthorizationCodeAuthorizationFilter(String tokenUrl, String clientId, String clientSecret, String code, URI redirectUri) {
		super(tokenUrl, clientId, clientSecret);
		
		this.code = StringUtils.trimToNull(code);
		this.redirectUri = redirectUri;
	}
	
	@Override
	protected Form createForm() {
        this.logger.debug("Using OAuth grant_type 'authorization_code'");
		Form form = new Form().param("grant_type", "authorization_code");
		if (this.redirectUri != null)
			form.param("redirect_uri", this.redirectUri.toString());
		return form;
	}
	
	@Override
	protected void extendFormSensitive(Form form) {
	    form.param("code", this.code);
	}

}
