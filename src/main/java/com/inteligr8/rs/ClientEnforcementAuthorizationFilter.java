/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.rs;

import jakarta.ws.rs.client.ClientRequestContext;

import org.apache.commons.lang3.StringUtils;

/**
 * This class is the base for implementations of client authorization similar
 * to OAuth-based flows.
 *  
 * @author brian@inteligr8.com
 */
public class ClientEnforcementAuthorizationFilter implements AuthorizationFilter {
	
	private final String clientId;
	private final String clientSecret;
	
	/**
	 * This constructor creates a client authorization filter using a client ID
	 * registered with the endpoint.
	 * 
	 * @param clientId An endpoint provided client ID.
	 */
	public ClientEnforcementAuthorizationFilter(String clientId) {
		this(clientId, null);
	}

	/**
	 * This constructor creates a client authorization filter using a client ID
	 * registered with the endpoint, and the corresponding client secret.
	 * 
	 * @param clientId An endpoint provided client ID.
	 * @param clientSecret A secret corresponding to the client ID.
	 */
	public ClientEnforcementAuthorizationFilter(String clientId, String clientSecret) {
		this.clientId = StringUtils.trimToNull(clientId);
		this.clientSecret = StringUtils.trimToNull(clientSecret);
	}

	/**
	 * This method applies the client headers to the {@link ClientRequestContext}.
	 * 
	 * @param requestContext A request context.
	 */
	@Override
	public void filter(ClientRequestContext requestContext) {
		requestContext.getHeaders().add("client_id", this.clientId);
		requestContext.getHeaders().add("client_secret", this.clientSecret);
	}

}
