/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.rs;

import java.util.Map;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status.Family;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.jakarta.rs.json.JacksonJsonProvider;

/**
 * This class is the base for implementations of OAuth authorization flows.
 *  
 * @author brian@inteligr8.com
 */
public abstract class OAuthAuthorizationFilter implements AuthorizationFilter {
	
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
	private final String tokenUrl;
	private final String clientId;
	private final String clientSecret;
	private final String scope;
	private String accessToken;
	private long expiration;
	private String refreshToken;
	private Long refreshTokenExpiration;
	
	/**
	 * This constructor creates an OAuth-based authorization filter using the
	 * OAuth identity provider token URL and a client ID registered with the
	 * same OAuth identity provider.
	 * 
	 * @param tokenUrl An OAuth identity provider token URL.
	 * @param clientId An OAuth identity provider client ID.
	 */
	public OAuthAuthorizationFilter(String tokenUrl, String clientId) {
		this(tokenUrl, clientId, null);
	}

	/**
	 * This constructor creates an OAuth-based authorization filter using the
	 * OAuth identity provider token URL, client ID registered with the
	 * same OAuth identity provider, and the corresponding client secret.
	 * 
	 * @param tokenUrl An OAuth identity provider token URL.
	 * @param clientId An OAuth identity provider client ID.
	 * @param clientSecret A secret corresponding to the client ID.
	 */
	public OAuthAuthorizationFilter(String tokenUrl, String clientId, String clientSecret) {
		this(tokenUrl, clientId, clientSecret, null);
	}

	/**
	 * This constructor creates an OAuth-based authorization filter using the
	 * OAuth identity provider token URL, client ID registered with the
	 * same OAuth identity provider, the corresponding client secret, and OAuth
	 * scope.
	 * 
	 * @param tokenUrl An OAuth identity provider token URL.
	 * @param clientId An OAuth identity provider client ID.
	 * @param clientSecret A secret corresponding to the client ID.
	 * @param scope An OAuth scope.
	 */
	public OAuthAuthorizationFilter(String tokenUrl, String clientId, String clientSecret, String scope) {
		this.tokenUrl = tokenUrl;
		this.clientId = StringUtils.trimToNull(clientId);
		this.clientSecret = StringUtils.trimToNull(clientSecret);
		this.scope = StringUtils.trimToNull(scope);
	}

	/**
	 * This method applies the 'Authorization' header to the {@link ClientRequestContext}.
	 * 
	 * @param requestContext A request context.
	 */
	@Override
	public void filter(ClientRequestContext requestContext) {
		if (this.accessToken == null) {
		    this.requestToken();
		} else if (System.currentTimeMillis() >= this.expiration) {
            this.logger.trace("Access token expired; retrieving new one with refresh token");
            
		    if (this.refreshTokenExpiration != null && System.currentTimeMillis() >= this.refreshTokenExpiration.longValue()) {
                this.logger.debug("Refresh token expired; performing full authentication");
                this.refreshToken = null;
                this.requestToken();
		    } else {
    		    try {
    		        this.requestToken();
                } catch (WebApplicationException wae) {
                    if (wae.getResponse().getStatusInfo().getFamily() == Family.CLIENT_ERROR) {
                        this.logger.debug("Received OAuth response {} using refresh token; performing full authentication", wae.getResponse().getStatus());
                        this.refreshToken = null;
                        this.requestToken();
                    } else {
                        throw wae;
                    }
                }
		    }
		}
		
		requestContext.getHeaders().add(HttpHeaders.AUTHORIZATION, "Bearer " + this.accessToken);
	}
	
	/**
	 * This method manages the acquisition and refreshing of tokens, per the
	 * standard OAuth process.
	 */
	private void requestToken() {
		Form form;
		
		if (this.refreshToken != null) {
			form = this.createRefreshForm();
		} else {
			form = this.createForm();
		}

		form.param("client_id", this.clientId);
		if (this.scope != null)
			form.param("scope", this.scope);
		
		this.logger.trace("Sending OAuth request: {}", form);
		
		if (this.refreshToken != null) {
		    this.extendRefreshFormSensitive(form);
		} else {
            this.extendFormSensitive(form);
		}

        if (this.clientSecret != null)
            form.param("client_secret", this.clientSecret);
		
		Entity<Form> entity = Entity.form(form);
		
		Client client = ClientBuilder.newBuilder()
                .register(new JacksonJsonProvider())
                .build();
		WebTarget target = client.target(this.tokenUrl);
		
		long requestSendTime = System.currentTimeMillis();

		Response response = target.request().post(entity);
		
        this.logger.debug("Received OAuth response: {}", response.getStatus());

        @SuppressWarnings("unchecked")
        Map<String, Object> responseMap = response.readEntity(Map.class);

        this.logger.trace("Received OAuth response: {}", responseMap);
		
		if (response.getStatusInfo().getFamily() != Family.SUCCESSFUL) {
            String code = (String) responseMap.get("error");
            if (code != null) {
                String description = (String) responseMap.get("error_description");
    			throw new WebApplicationException(code + ": " + description, response.getStatus());
            } else {
                throw new WebApplicationException(response);
            }
		}
		
		this.accessToken = (String)responseMap.get("access_token");
		this.expiration = requestSendTime + ((Number)responseMap.get("expires_in")).longValue() * 1000L;
		this.refreshToken = (String)responseMap.get("refresh_token");
		if (responseMap.containsKey("refresh_token_expires_in"))
		    this.refreshTokenExpiration = requestSendTime + ((Number)responseMap.get("refresh_token_expires_in")).longValue() * 1000L;
	}
	
	protected Form createRefreshForm() {
		return new Form().param("grant_type", "refresh_token")
				.param("refresh_token", this.refreshToken);
	}
	
	protected abstract Form createForm();
	
	protected void extendRefreshFormSensitive(Form form) {
	}

    protected abstract void extendFormSensitive(Form form);

}
