/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.rs;

import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.core.HttpHeaders;

/**
 * This class implements a simple long living or proxied token-based
 * authorization filter.  The token is expected to be acquired outside of the
 * purview of this library.
 * 
 * If you have the full authorization header and not just the bearer token, use
 * the {@link ForwardingAuthorizationFilter}.
 *  
 * @author brian@inteligr8.com
 */
public class BearerTokenAuthorizationFilter implements AuthorizationFilter {
	
	private final String token;
	
	/**
	 * This constructor instantiates the filter with required fields.
	 * 
	 * @param token A 'Bearer' token.
	 */
	public BearerTokenAuthorizationFilter(String token) {
		this.token = token;
	}

	/**
	 * This method applies the 'Authorization' header to the {@link ClientRequestContext}.
	 * 
	 * @param requestContext A request context.
	 */
	@Override
	public void filter(ClientRequestContext requestContext) {
		requestContext.getHeaders().add(HttpHeaders.AUTHORIZATION, "Bearer " + this.token);
	}

}
