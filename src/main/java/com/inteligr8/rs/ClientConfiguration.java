/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.rs;

import java.net.URI;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jakarta.rs.json.JacksonJsonProvider;

import jakarta.ws.rs.client.ClientBuilder;

/**
 * This interface defines the configurable parameters of the clients; primarily
 * their default authentication and authorization.
 * 
 * @author brian@inteligr8.com
 */
public interface ClientConfiguration {
	
	/**
	 * This method retrieves the base/root URL of the client service.
	 * 
	 * @return The URL.
	 */
	String getBaseUrl();
	
	/**
	 * This method retrieves the username to use in HTTP BASIC authentication/authorization.
	 * 
	 * @return A username.
	 */
	default String getBasicAuthUsername() {
		return null;
	}

	/**
	 * This method retrieves the password to use in HTTP BASIC authentication/authorization.
	 * 
	 * @return The corresponding password for the username.
	 */
	default String getBasicAuthPassword() {
		return null;
	}
	
	/**
	 * This method retrieves the client identifier to use in Client Enforcement authorization.
	 * 
	 * @return A client identifier.
	 */
	default String getClientId() {
		return null;
	}

	/**
	 * This method retrieves the client secret to use in Client Enforcement authorization.
	 * 
	 * @return The corresponding client secret for the client identifier.
	 */
	default String getClientSecret() {
		return null;
	}
	
	/**
	 * This method retrieves the token to use in HTTP BEARER authorization.
	 * This is provided in a response to the token URL.
	 * 
	 * @return An access token.
	 */
	default String getBearerToken() {
		return null;
	}
	
	/**
	 * This method retrieves the token URL to use for OAuth authorization.
	 * The value can be pulled from OAuth endpoint well-known meta-data.  That
	 * endpoint or the token URL itself may also be provided OAuth IdP
	 * administrator.
	 * 
	 * @return An OAuth token URL.
	 */
	default String getOAuthTokenUrl() {
		return null;
	}
	
	/**
	 * This method retrieves the client identifier to use in OAuth
	 * authorization.  This is provided by the OAuth IdP administrator or
	 * tooling.
	 * 
	 * @return A client identifier.
	 */
	default String getOAuthClientId() {
		return this.getClientId();
	}

	/**
	 * This method retrieves the client secret to use in OAuth authorization.
	 * This is provided by the OAuth IdP administrator or tooling.
	 * 
	 * @return The corresponding client secret for the client identifier.
	 */
	default String getOAuthClientSecret() {
		return this.getClientSecret();
	}
	
	/**
	 * This method retrieves the authorization code to use in OAuth
	 * Authorization Code flow.  This is provided by the OAuth IdP
	 * administrator or tooling.
	 * 
	 * @return An authorization code.
	 */
	default String getOAuthAuthCode() {
		return null;
	}
	
	/**
	 * This method retrieves the redirect URL to use in OAuth Authorization
	 * Code flow.  This has meaning to the client-side web application.
	 * 
	 * @return A URL for the OAuth flow to redirect to when complete.
	 */
	default String getOAuthAuthRedirectUri() {
		return null;
	}
	
	/**
	 * This method retrieves the username to use in OAuth Password Grant flow.
	 * This is provided by the OAuth IdP administrator or tooling.
	 * 
	 * @return A username.
	 */
	default String getOAuthUsername() {
		return null;
	}
	
	/**
	 * This method retrieves the password to use in OAuth Password Grant flow.
	 * This is provided by the OAuth IdP administrator or tooling.
	 * 
	 * @return The corresponding password for the username.
	 */
	default String getOAuthPassword() {
		return null;
	}
	
	
	
	/**
	 * This method retrieves the connection (before request sent) timeout for
	 * the client.
	 * 
	 * @return A timeout in milliseconds.
	 */
	default Integer getConnectTimeoutInMillis() {
	    return null;
	}
	
	/**
	 * This method retrieves the response (after request sent) timeout for the
	 * client.
	 * 
	 * @return A timeout in milliseconds.
	 */
	default Integer getResponseTimeoutInMillis() {
	    return null;
	}
	
	

	/**
	 * This method enables/disables the JackSON UNWRAP_ROOT_VALUE feature.
	 * 
	 * @return `true` to enable; `false` otherwise.
	 */
	default boolean isUnwrapRootValueEnabled() {
		return false;
	}

	/**
	 * This method enables/disables the JackSON WRAP_ROOT_VALUE feature.
	 * 
	 * @return `true` to enable; `false` otherwise.
	 */
	default boolean isWrapRootValueEnabled() {
		return false;
	}
    
    /**
	 * This method allows sub-classes to extend the JackSON mapper
	 * configuration and behavior.
	 * 
     * @param mapper A JackSON object mapper.
     */
    default void configureJacksonMapper(ObjectMapper mapper) {
    }
	
	/**
	 * This method allows sub-classes to extend the JackSON JSON provider.
	 * 
	 * @param provider A JackSON Jakarta RS provider.
	 */
	default void configureJacksonProvider(JacksonJsonProvider provider) {
	}
	
	

	/**
	 * This method creates an authorization filter based on the configuration
	 * available.  A configuration element is considered to not be available
	 * when its value is null.  If multiple configurations are specified, the
	 * filter is selected with the following precedence.
	 * 
	 * - Bearer
	 * - OAuth Authorization Code
	 * - OAuth Password Grant
	 * - OAuth Client Credential
	 * - Basic
	 * 
	 * @return An authorization filter; may be null
	 */
	default AuthorizationFilter createAuthorizationFilter() {
		if (this.getBearerToken() != null) {
			return new BearerTokenAuthorizationFilter(this.getBearerToken());
		} else if (this.getOAuthTokenUrl() != null) {
			if (this.getOAuthAuthCode() != null) {
				return new OAuthAuthorizationCodeAuthorizationFilter(this.getOAuthTokenUrl(),
						this.getOAuthClientId(), this.getOAuthClientSecret(),
						this.getOAuthAuthCode(), URI.create(this.getOAuthAuthRedirectUri()));
			} else if (this.getOAuthUsername() != null) {
				return new OAuthPasswordGrantAuthorizationFilter(this.getOAuthTokenUrl(),
						this.getOAuthClientId(), this.getOAuthClientSecret(),
						this.getOAuthUsername(), this.getOAuthPassword());
			} else {
				return new OAuthClientCredentialAuthorizationFilter(this.getOAuthTokenUrl(),
						this.getOAuthClientId(), this.getOAuthClientSecret());
			}
		} else if (this.getClientId() != null) {
			return new ClientEnforcementAuthorizationFilter(this.getClientId(), this.getClientSecret());
		} else if (this.getBasicAuthUsername() != null) {
			return new BasicAuthorizationFilter(this.getBasicAuthUsername(), this.getBasicAuthPassword());
		} else {
			return null;
		}
	}
    
    /**
     * A Jackson provider, logging filter, and authentication filter are already registered.
     * 
     * @param clientBuilder A JAX-RS client builder to configure.
     */
    default void configureClient(ClientBuilder clientBuilder) {
    }

}
