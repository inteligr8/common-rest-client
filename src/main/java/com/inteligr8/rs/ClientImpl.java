/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.rs;

import jakarta.annotation.PostConstruct;
import jakarta.ws.rs.client.ClientBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class that provides pre-configured Jakarta RS Client &amp; WebTarget
 * objects.
 * 
 * @author brian@inteligr8.com
 */
public class ClientImpl extends Client {
	
	private final Logger logger = LoggerFactory.getLogger(ClientImpl.class);
	
	private ClientConfiguration config;
	
	/**
	 * This constructor is for Spring or POJO use.
	 * 
	 * @param config The client configuration.
	 */
	public ClientImpl(ClientConfiguration config) {
		this.config = config;
	}

	/**
	 * This method is a placeholder.
	 */
	@PostConstruct
	public void register() {
		this.logger.info("API Base URL: {}", this.getConfig().getBaseUrl());
	}
	
	@Override
	public void buildClient(ClientBuilder clientBuilder) {
	}
	
	@Override
	public ClientConfiguration getConfig() {
		return this.config;
	}

	@Override
	public <T> T getApi(AuthorizationFilter authFilter, Class<T> apiClass) {
		throw new UnsupportedOperationException();
	}

}
