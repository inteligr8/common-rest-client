/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.rs;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import jakarta.ws.rs.client.ClientRequestContext;

/**
 * This class implements a header-based authorization filter.
 *  
 * @author brian@inteligr8.com
 */
public class HeaderAuthorizationFilter implements AuthorizationFilter {
	
	private final MultiValueMap<String, Object> headers = new LinkedMultiValueMap<>();
	
	/**
	 * This constructor instantiates the filter with required fields.
	 * 
	 * @param headerName A header name.
	 * @param headerValue A header value.
	 */
    public HeaderAuthorizationFilter(String headerName, Object headerValue) {
	    this.headers.add(headerName, headerValue);
	}
    
	/**
	 * This method adds another header name/value to outgoing requests.
	 * 
	 * @param headerName A header name.
	 * @param headerValue A header value.
	 * @return This class for fluent chaining.
	 */
    public HeaderAuthorizationFilter add(String headerName, Object headerValue) {
        this.headers.add(headerName, headerValue);
        return this;
    }
	
	/**
	 * This method applies the 'Authorization' header to the {@link ClientRequestContext}.
	 * 
	 * @param requestContext A request context.
	 */
	@Override
	public void filter(ClientRequestContext requestContext) throws UnsupportedEncodingException {
	    for (Entry<String, List<Object>> header : this.headers.entrySet())
	        requestContext.getHeaders().put(header.getKey(), header.getValue());
	}

}
