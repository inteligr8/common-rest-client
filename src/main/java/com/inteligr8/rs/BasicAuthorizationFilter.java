/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.rs;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.core.HttpHeaders;

import org.apache.commons.lang3.StringUtils;

/**
 * This class implements a simple 2-credential (username &amp; password) based
 * authorization filter.
 *  
 * @author brian@inteligr8.com
 */
public class BasicAuthorizationFilter implements AuthorizationFilter {
	
	private final String username;
	private final String password;
	
	/**
	 * This constructor instantiates the filter with required fields.
	 * 
	 * @param username A username or access key.
	 * @param password A password or secret key.
	 */
	public BasicAuthorizationFilter(String username, String password) {
		this.username = StringUtils.trimToNull(username);
		this.password = StringUtils.trimToNull(password);
	}
	
	/**
	 * This method applies the 'Authorization' header to the {@link ClientRequestContext}.
	 * 
	 * @param requestContext A request context.
	 * @throws UnsupportedEncodingException The 'utf-8' encoding is not supported.
	 */
	@Override
	public void filter(ClientRequestContext requestContext) throws UnsupportedEncodingException {
		String userAndPass = this.username + ":" + this.password;
		String userAndPassEncoded = Base64.getEncoder().encodeToString(userAndPass.getBytes("utf-8"));
		
		requestContext.getHeaders().add(HttpHeaders.AUTHORIZATION, "Basic " + userAndPassEncoded);
	}

}
