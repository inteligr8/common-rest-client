/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.rs;

import jakarta.ws.rs.core.Form;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the OAuth Password Grant flow as an authorization
 * filter.
 * 
 * @author brian@inteligr8.com
 */
public class OAuthPasswordGrantAuthorizationFilter extends OAuthAuthorizationFilter {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final String username;
	private final String password;

	/**
	 * @param tokenUrl The URL to the OAuth IdP token service.
	 * @param clientId The ID provided by the OAuth IdP administrator.
	 * @param username A username provided by either the OAuth IdP administrator or one of its integrated providers.
	 * @param password The corresponding password for the username parameter.
	 */
	public OAuthPasswordGrantAuthorizationFilter(String tokenUrl, String clientId, String username, String password) {
		this(tokenUrl, clientId, null, username, password);
	}

	/**
	 * @param tokenUrl The URL to the OAuth IdP token service.
	 * @param clientId The ID provided by the OAuth IdP administrator.
	 * @param clientSecret The passcode provided by the OAuth IdP administrator.
	 * @param username A username provided by either the OAuth IdP administrator or one of its integrated providers.
	 * @param password The corresponding password for the username parameter.
	 */
	public OAuthPasswordGrantAuthorizationFilter(String tokenUrl, String clientId, String clientSecret, String username, String password) {
		super(tokenUrl, clientId, clientSecret);
		this.username = StringUtils.trimToNull(username);
		this.password = StringUtils.trimToNull(password);
	}
	
	@Override
	protected Form createForm() {
        this.logger.debug("Using OAuth grant_type 'password': {}", this.username);
		return new Form().param("grant_type", "password")
				.param("username", this.username);
	}
	
	@Override
	protected void extendFormSensitive(Form form) {
	    form.param("password", this.password);
	}

}
