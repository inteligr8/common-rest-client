/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.rs;

import java.io.IOException;

import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.client.ClientRequestFilter;
import jakarta.ws.rs.client.ClientResponseContext;
import jakarta.ws.rs.client.ClientResponseFilter;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This is an JAX-RS filter for the logging of any JAX-RS request/response for
 * debugging purposes.  When used, it will write to 'jaxrs.request' and
 * 'jaxrs.response' loggers at the 'trace' level.
 * 
 * @author brian@inteligr8.com
 */
public class LoggingFilter implements ClientRequestFilter, ClientResponseFilter {
	
	private final Logger loggerRequest = LoggerFactory.getLogger("jaxrs.request");
	private final Logger loggerResponse = LoggerFactory.getLogger("jaxrs.response");
	
	protected final ObjectMapper om = new ObjectMapper().setSerializationInclusion(Include.NON_NULL);
	
	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		if (this.loggerRequest.isTraceEnabled())
			this.logAnyRequest(requestContext, this.loggerRequest);
	}
	
	protected void logAnyRequest(ClientRequestContext requestContext, Logger logger) throws IOException {
		if (MediaType.APPLICATION_JSON_TYPE.equals(requestContext.getMediaType())) {
			logger.trace("request: {} {}: {}", requestContext.getMethod(), requestContext.getUri(),
					this.om.writeValueAsString(requestContext.getEntity()));
		} else if (MediaType.APPLICATION_FORM_URLENCODED_TYPE.equals(requestContext.getMediaType())) {
			if (requestContext.getEntity() instanceof Form) {
				logger.trace("request: {} {}: {}", requestContext.getMethod(), requestContext.getUri(),
						((Form)requestContext.getEntity()).asMap());
			} else {
				logger.trace("request: {} {}: failed to output form", requestContext.getMethod(), requestContext.getUri());
			}
		} else {
			this.logUnhandledRequest(requestContext, logger);
		}
	}
	
	protected void logUnhandledRequest(ClientRequestContext requestContext, Logger logger) throws IOException {
		if (requestContext.getMediaType() != null) {
			logger.trace("request '{}': {} {}", requestContext.getMediaType(), requestContext.getMethod(), requestContext.getUri());
		} else {
			logger.trace("request: {} {}", requestContext.getMethod(), requestContext.getUri());
		}
		
	}
	
	@Override
	public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
		if (this.loggerResponse.isTraceEnabled()) {
			this.loggerResponse.trace("response: {} ", this.om.writeValueAsString(responseContext.getStatus()));
			// WARN body is stream, which would need to be replaced after read
			this.loggerResponse.warn("response: NOT YET SUPPORTED");
		}
	}

}
